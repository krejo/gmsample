# README #

While interviewing for a position at GM they requested candidates answer three questions.

Questions required:
 
1) Describe your experience with TDD and unit testing frameworks.

2) Describe how you would implement a screen to list dealerships near a customer. Include objective-c code implementing this screen.

3) Describe how you would connect to a web service from an app and retrieve data. Provide sample objective-c code retrieving data from a restful web service of your choice.

### Purpose ###

This app demonstrates the use of Location services

* To demonstrate competence
* Locate GM Dealerships close by
* Uses googleapis as a web service Example

### Setup ###

* Clone the repo
* Open in Xcode7
* 


## Response ##

### (1) Describe your experience with TDD and unit testing frameworks. ###

The organizations I have worked for did not embrace Test Driven Development. My TDD experience comes from personal experience on private apps.
I have used XCTest from Xcode
Recently, I developed an object for an audio app using the Unit Test framework in Xcode.  This was done purposefully as I did not want to create the Object in the app and experiment with it - Instead, I knew what the results should be given specified Input so I used the Unit Test to full develop the Object.

The Audio app allowed multiple tracks to be played and an each track could be clipped at both ends and started at a user specified time.  This object was called a ‘TrackReference’ and would specify a left inset and right inset for the clipping and a startTime relative for all track playback.  The object would compute 1) where to start reading in the audioFile and 2) how much delay to start playing.

The biggest thing I appreciate about TDD is that only code for the requirement is implemented, and that ‘extra’ code is avoided. Extra code that might be beneficial in future implementation is not considered.  Too often inexperienced developers like to create grand Objects that do ‘so much’, but the extra  effort to implement and test is not worth it.

### (2) Describe how you would implement a screen to list dealerships near a customer. ###

Include objective-c code implementing this screen.

For starters, the location of the customer has to be determined.
In iOS, an app first must requestAuthorization from the User to use location, and a description, of some sort, of what the location will be used for should be given.

Use CLLocationManager from CoreLocation to request the location. A delegate to the manager will be notified of the result location.

Use MKLocalSearchRequest from MapKit to Request  a search for places matching “GM Dealership”
Upon completion of the request an array of places is returned and listed on a tableView

This technique uses Apple’s search in MapKit, Google has a similar search.
A more accurate approach would probably be that GM implement a WebService that given a location return result locations, say within a 100 Miles. And given a Lat/Long for each location further filtering can take place, Say within 10 miles or 50 miles. This would be distance between points.  Driving or Walking distance and time estimates would be more complex as routes would need to be computed.

### (3) Describe how you would connect to a web service from an app and retrieve data. ###

Provide sample objective-c code retrieving data from a restful web service of your choice.

An HTTP Request is submitted.  In versions of iOS previous to iOS9 this typically was a an NSURL connection, in iOS9 uses an NSURLSession and its support objects like NSURLSessionDataTask
The URL of the request contains a path to a resource on the server. Depending on the services implementation the appropriate HTTPMethod GET, PUT, POST is used
HTTP request also has headers and a body that can contain additional information.

The Body may be JSON holding additional data needed for the request.
The Headers may contain tokens, passwords and other client identifying information

In My example I used google’s Youtube API

url: 'https://www.googleapis.com/youtube/v3/search?'
part=snippet&q=General%20Motors%20Assembly%20Line&key=AIzaSyDZX_Y5M0XIp69bPqOwM0fezYpSwQ2oUdg&type=video&pageToken=CAoQAA

It uses the GET HTTPMethod as documented by the API and makes sense which we are retrieving search results for videos matching a String
In my case “General Motors Assembly”

The web service is ‘youtube/v3/search’

A parameter tells what type of info i am looking for : ‘snippet’
?part=snippet

On the URL we also provide the search query  &q=
an Api Key given to developers of the api   &key=
a type of search  &type=
and a Page Token  &pageToken=

The results come back in JSON and are parsed as an Array of snippets (dictionary info)

The results of Webservices are typically in JSON or XML format. Nonetheless, they typically get converted to internal Objects and Containers, NSArray, NSDictionary, Class Objects and ManagedObjects from CoreData





### Who do I talk to? ###

* Repo owner or admin