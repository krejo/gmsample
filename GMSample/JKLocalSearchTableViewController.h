//
//  JKLocalSearchTableViewController.h
//  GMSample
//
//  Created by JOSEPH KERR on 5/12/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface JKLocalSearchTableViewController : UITableViewController <CLLocationManagerDelegate>

@property (nonatomic, strong) NSArray *places;

@end

