//
//  JKLocalSearchTableViewController.m
//  GMSample
//
//  Created by JOSEPH KERR on 5/12/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

#import "JKLocalSearchTableViewController.h"
#import <MapKit/MapKit.h>

@interface JKLocalSearchTableViewController () {
    MKCoordinateRegion searchRegion;
}
@property (nonatomic, strong) MKLocalSearch *localSearch;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic) CLLocationCoordinate2D userLocation;
@end

@implementation JKLocalSearchTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [self checkLocationServices];
}

#pragma mark -

- (void)searchForDealerships
{
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = self.userLocation.latitude;
    newRegion.center.longitude = self.userLocation.longitude;
    
    newRegion.span.latitudeDelta  = 0.032872;
    newRegion.span.longitudeDelta = 0.028986;
    
    MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
    
    request.naturalLanguageQuery = @"GM Dealership";
    
    request.region = newRegion;
    
    MKLocalSearchCompletionHandler completionHandler = ^(MKLocalSearchResponse *response, NSError *error)
    {
        if (error != nil)
        {
            NSString *errorStr = [[error userInfo] valueForKey:NSLocalizedDescriptionKey];
            NSLog(@"error search %@",errorStr);
        }
        else
        {
            self.places = [response mapItems];
            
            [self.tableView reloadData];
        }
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    };
    
    
    self.localSearch = [[MKLocalSearch alloc] initWithRequest:request];
    
    [self.localSearch startWithCompletionHandler:completionHandler];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

    
    // Set Searchregion used by map
    
    searchRegion.center.latitude = newRegion.center.latitude;
    searchRegion.center.longitude  = newRegion.center.longitude;
    searchRegion.span.latitudeDelta  = newRegion.span.latitudeDelta;
    searchRegion.span.longitudeDelta = newRegion.span.longitudeDelta;
}

#pragma mark -

- (void)checkLocationServices
{
    NSString *causeStr = nil;
    
    if ([CLLocationManager locationServicesEnabled] == NO)
    {
        causeStr = @"Device";
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        causeStr = @"App";
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined)
    {
        [self.locationManager requestWhenInUseAuthorization];
        
        NSLog(@"NotDetermined, requestWhenInUseAuthorization");
    }
    else
    {
        [self.locationManager requestLocation];
        
//        [self.locationManager startUpdatingLocation];
    }
    
    if (causeStr != nil)
    {
        NSString *alertMessage = [NSString stringWithFormat:
                                  @"location services disabled for this %@ use \"Settings\" app to turn on Location Services.", causeStr];
        
        NSLog(@"location services not available %@",alertMessage);
    }
    
}

#pragma mark - CLLocationManagerDelegate methods

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        NSLog(@"WhenInUse is what we want %d",status);
        [self.locationManager requestLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray*)locations
{
    self.userLocation = [(CLLocation *)[locations lastObject] coordinate];
    
    [self searchForDealerships];
    
    //[manager stopUpdatingLocation];
    
    manager.delegate = nil;
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"error %@",[error description]);
}

#pragma mark - UITableView delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.places count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"JKSearchResultCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    MKMapItem *mapItem = [self.places objectAtIndex:indexPath.row];
    
    cell.textLabel.text = mapItem.name;
    cell.detailTextLabel.text = mapItem.phoneNumber;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [MKMapItem openMapsWithItems:self.places
                   launchOptions:@{
                                   MKLaunchOptionsMapCenterKey : [NSValue valueWithMKCoordinate:searchRegion.center],
                                   MKLaunchOptionsMapSpanKey: [NSValue valueWithMKCoordinateSpan:searchRegion.span]
                                   }
     ];
}

@end

