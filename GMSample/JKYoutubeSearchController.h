//
//  JKYoutubeSearchController.h
//  GMSample
//
//  Created by JOSEPH KERR on 5/12/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JKYoutubeSearchController : NSObject

-(void)getSearchKeywordDetailsWithString:(NSString*)searchStr
                               newSearch:(BOOL)newSearch
                            onCompletion:(void (^)(NSMutableArray* channelData,NSString *searchStr))completion;

-(void)initWebDataKeyWithSearchString:(NSString *)searchQuery newSearch:(BOOL)newSearch;

@end
