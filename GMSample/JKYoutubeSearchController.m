//
//  JKYoutubeSearchController.m
//  GMSample
//
//  Created by JOSEPH KERR on 5/12/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

#import "JKYoutubeSearchController.h"

/*
 This class uses the YouTube API to search for videos that match certian criteria.  Basically all this class does is perform a
 HTTP GET request on the googleapis/youtube URL and once the request is complete and an HTTP Response of 200 is achieved
 (success), a completion block is called and the JSON style data that is recieved is placed in an array of dictionaries.
 
 */

@interface JKYoutubeSearchController() {
    id _nextPageToken;
}
@property (nonatomic) NSString* apiKey;
@property (nonatomic) NSString* searchString;
@property (nonatomic) NSArray* desiredChannelsArray;
@property (nonatomic) NSInteger channelIndex;
@property (nonatomic) NSMutableArray* searchResults;
@property (nonatomic) NSMutableDictionary* channelData;
@property (nonatomic) NSUInteger totalResults;
@end

const NSString *kYoutubeAPIKey = @"AIzaSyDZX_Y5M0XIp69bPqOwM0fezYpSwQ2oUdg";

@implementation JKYoutubeSearchController

-(NSMutableArray *)searchResults {
    
    if (! _searchResults) {
        _searchResults = [NSMutableArray new];
    }
    return _searchResults;
}

-(void)initWebDataKeyWithSearchString:(NSString *)searchQuery newSearch:(BOOL)newSearch {
    
    self.searchString = searchQuery;
    
    if (newSearch) {
        _totalResults = 0;
        [_searchResults removeAllObjects];
        _nextPageToken = nil;
    }
    
}

-(void)webserviceGet:(NSURL *)url
         completionHandler:(void (^)(NSData * data, NSURLResponse* response, NSError* error))completion {
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    
    request.HTTPMethod = @"GET";
    
    // Use the defaultSessionConfiguration
    
    NSURLSession* session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionDataTask* task = [session dataTaskWithRequest:request completionHandler:^(NSData* data, NSURLResponse* response, NSError* error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(data, response, error);
        });
    }];
    
    [task resume];
}


// Here, we pass the raw searchstring not the modified one so we can give it back on completion to identify if it
// is part of the current search

-(void)getSearchKeywordDetailsWithString:(NSString*)searchStr
                               newSearch:(BOOL)newSearch
                            onCompletion:(void (^)(NSMutableArray* channelData,NSString* searchStr))completion {
    
    NSLog(@"_nextPageToken %@",_nextPageToken ? @"NEXTPAGE" : @"NEW SEARCH");
    if (newSearch) {
        [_searchResults removeAllObjects];
        _nextPageToken = nil;
    }
    
    NSString* urlString;

    if (!_nextPageToken) {
        urlString = [NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/search?part=snippet&q=%@&key=%@&type=video",
                     self.searchString, kYoutubeAPIKey];
    }
    else {
        urlString = [NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/search?part=snippet&q=%@&key=%@&type=video&pageToken=%@",
                     self.searchString, kYoutubeAPIKey, (NSString*)_nextPageToken];
    }
    
    NSURL* targetURL = [NSURL URLWithString:urlString];
    

    NSLog(@"url: %@",[targetURL description]);
    
    [self webserviceGet:targetURL completionHandler:^(NSData* data, NSURLResponse* HTTPResponse, NSError* error) {
        
        if ([HTTPResponse isKindOfClass:[NSHTTPURLResponse class]]) {
            
            if ([(NSHTTPURLResponse*) HTTPResponse statusCode] == 200 && !error) {
                
                NSMutableArray *result = [@[] mutableCopy];
                
                id responseData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                
                id pageInfo =[responseData objectForKey:@"pageInfo"];
                
                NSUInteger resultsPerPage = [pageInfo[@"resultsPerPage"] unsignedIntegerValue];
                NSUInteger lastOfPageIndex = resultsPerPage -1;
                
                _nextPageToken = [responseData valueForKey:@"nextPageToken"];
                
                NSArray *items = [responseData objectForKey:@"items"];
                
                NSUInteger counter = 0;
                NSUInteger minimumResults = 14;
                
                BOOL resultsAdded = NO;
                
                for (id item in items) {
                    
                    NSMutableDictionary *singleItemResult = [@{} mutableCopy];
                    singleItemResult[@"ytvideoid"] = item[@"id"][@"videoId"];   //  key videoID  == ytvideoid
                    singleItemResult[@"videoTitle"] = item[@"snippet"][@"title"];
                    singleItemResult[@"thumbnail"] = item[@"snippet"][@"thumbnails"][@"default"][@"url"];
                    singleItemResult[@"ytdescriptionsnippet"] = item[@"snippet"][@"description"];
                    
                    [result addObject:singleItemResult];
                    
                    // check last item on page
                    
                    NSUInteger totalResultsSoFar = self.searchResults.count + result.count;
                    
                    // NSLog(@"totalResultsSoFar %ld",totalResultsSoFar);
                    
                    if (counter == lastOfPageIndex ){
                        resultsAdded = YES;
                        [_searchResults addObjectsFromArray:result];
                        
                        if (totalResultsSoFar < minimumResults) {
                            NSLog(@"LAST of Page and CONTINUE results");
                            [self getSearchKeywordDetailsWithString:(NSString*)searchStr newSearch:(BOOL)NO onCompletion:completion];
                            
                        } else {
                            
                            NSLog(@"LAST of Page and RETURN results");
                            completion(_searchResults,searchStr);
                        }
                    }
                    
                    counter++;
                    if (counter < resultsPerPage) {
                        continue;
                    } else {
                        break;
                    }
                    
                }  // for
                
                if (!resultsAdded) {
                    [_searchResults addObjectsFromArray:result];
                }
                
            } else {
                NSLog(@"HTTP Status Code = %ld", [(NSHTTPURLResponse*) HTTPResponse statusCode] );
                NSLog(@"Error while loading channel details: %@", error);
            }
        }
        
        NSLog(@"Completed Task");
    }];
    
    
}

@end

