//
//  JKYoutubeTableViewController.h
//  GMSample
//
//  Created by JOSEPH KERR on 5/12/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

#import <UIKit/UIKit.h>


#import <UIKit/UIKit.h>

@protocol JKSearchTypingDelegate;


@interface JKYoutubeTableViewController : UITableViewController

@property (nonatomic,weak) id <JKSearchTypingDelegate> delegate;
@property (nonatomic) NSString *searchTerm;

@end


@protocol JKSearchTypingDelegate <NSObject>
@optional
-(void)searchTermChanged:(JKYoutubeTableViewController *)controller;


//-(void)finishedTrim:(JWYTSearchTypingViewController *)controller;
//-(void)finishedTrim:(JWYTSearchTypingViewController *)controller withDBKey:(NSString*)key;
//-(void)finishedTrim:(JWYTSearchTypingViewController *)controller title:(NSString*)title withDBKey:(NSString*)key;
@end
