//
//  ViewController.m
//  GMSample
//
//  Created by JOSEPH KERR on 5/12/16.
//  Copyright © 2016 JOSEPH KERR. All rights reserved.
//

#import "ViewController.h"
#import "JKYoutubeSearchController.h"

@interface ViewController () {
    NSString* _youtubeLinkWithoutId;
    BOOL _newSearchResults;
    dispatch_queue_t _imageRetrievalQueue;
}
@property (nonatomic) JKYoutubeSearchController* youTubeData;
@property (nonatomic) NSMutableArray* searchResults;

@property (weak, nonatomic) IBOutlet UIImageView *iv1;
@property (weak, nonatomic) IBOutlet UIImageView *iv2;
@property (weak, nonatomic) IBOutlet UIImageView *iv3;
@property (weak, nonatomic) IBOutlet UIImageView *iv4;
@property (weak, nonatomic) IBOutlet UIImageView *iv5;
@property (weak, nonatomic) IBOutlet UIImageView *iv6;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _youTubeData = [JKYoutubeSearchController new];
    _youtubeLinkWithoutId = @"http://youtu.be/";
    
    _newSearchResults = YES;
    _imageRetrievalQueue =
    dispatch_queue_create("imageProcessing",
                          dispatch_queue_attr_make_with_qos_class(DISPATCH_QUEUE_CONCURRENT,QOS_CLASS_USER_INTERACTIVE, 0));
}

- (void)viewDidAppear:(BOOL)animated {
    
    [self getYoutubeDataFromSearchText:@"General Motors Assembly Line"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#define TRACEIMAGE

#pragma mark -

-(void)getYoutubeDataFromSearchText:(NSString *)text {
    
    NSString *modifiedString = (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                              NULL,(CFStringRef)text,
                                                              NULL,(CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8 ));
    
    
    [self.youTubeData initWebDataKeyWithSearchString:modifiedString newSearch:_newSearchResults];
    
    [self.youTubeData getSearchKeywordDetailsWithString:text newSearch:_newSearchResults onCompletion:
     
     ^(NSMutableArray* channelData,NSString* searchStr) {
         
         //        NSUInteger count = [self.channelsDataArray count];  // get the index before adding
         
         if ([searchStr isEqualToString:text]) {
             
#ifdef TRACEIMAGE
             for (id channelItem in channelData) {
                 NSLog(@"%@",[channelItem valueForKey:@"ytvideoid"]);
             }
#endif
             @synchronized(_searchResults) {
                 _searchResults = channelData;
             }
             
             NSUInteger retrieveCount = 0;
             NSUInteger index = 0;
             @synchronized(_searchResults) {
                 
                 index = 0;
                 
                 for (id channelItem in _searchResults) {
                     
                     NSString *youtubeVideoId = [channelItem valueForKey:@"ytvideoid"];
#ifdef TRACEIMAGE
                     NSLog(@"retrieve %ld %@ image %@",index,youtubeVideoId,[channelItem valueForKey:@"thumbnail"]);
#endif
                     retrieveCount++;
                     
                     dispatch_async(_imageRetrievalQueue, ^{
                         UIImage* youtubeThumb = [UIImage imageWithData:[NSData dataWithContentsOfURL:
                                                                         [NSURL URLWithString:[channelItem valueForKey:@"thumbnail"]]]];
                         dispatch_async(dispatch_get_main_queue(), ^{
                             NSUInteger index = NSNotFound;
                             
                             @synchronized(_searchResults) {
                                 index = [_searchResults indexOfObjectPassingTest:
                                          ^BOOL(NSDictionary *dict, NSUInteger idx, BOOL *stop){
                                              return [[dict objectForKey:@"ytvideoid"] isEqualToString:youtubeVideoId];
                                          }];
                             }
                             if (index == NSNotFound) {
                                 NSLog(@"%s delete image",__func__);
                             } else{
                                 NSLog(@"%s keep image",__func__);
                                 
                                 if (index == 0) {
                                     _iv1.image = youtubeThumb;
                                 } else if (index == 1) {
                                     _iv2.image = youtubeThumb;
                                 } else if (index == 2) {
                                     _iv3.image = youtubeThumb;
                                 } else if (index == 3) {
                                     _iv4.image = youtubeThumb;
                                 } else if (index == 4) {
                                     _iv5.image = youtubeThumb;
                                 } else if (index == 5) {
                                     _iv6.image = youtubeThumb;
                                 }
                             }
                             
                         });
                     });
                     
                     index++;
                     
                 }  // for
                 
             } // synchronozed
             
             NSLog(@"%ld retrieve images",retrieveCount);
             
             
         } else {
             // Not interested in these results
             NSLog(@"Searchstr is DIFFERENT");
         }
     }];
}


@end


//NSCharacterSet *unallowedChars = [NSCharacterSet characterSetWithCharactersInString:@"!*'();:@&=+$,/?%#[]"];
//NSString *modifiedString = [text stringByAddingPercentEncodingWithAllowedCharacters:!unallowedChars];

